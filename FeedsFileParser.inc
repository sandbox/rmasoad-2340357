<?php

/**
 * @files
 * Provides the FeedsFileParser class.
 */
class FeedsFileParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $file = $fetcher_result->getFilePath();
    $file_realpath = drupal_realpath($file);
    $path_parts = pathinfo($file_realpath);

    $item = array(
      'file' => $file,
      'file_realpath' => $file_realpath,
      'filename' => $path_parts['filename'],
      'basename' => $path_parts['basename'],
      'extension' => $path_parts['extension'],
    );

    $result = new FeedsParserResult();
    $result->items[] = $item;
    return $result;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   */
  public function getMappingSources() {
    return parent::getMappingSources() + array(
      'file' => array(
        'name' => t('Drupal File Path'),
      ),
      'file_realpath' => array(
        'name' => t('File Path'),
      ),
      'filename' => array(
        'name' => t('Filename'),
      ),
      'basename' => array(
        'name' => t('Basename'),
      ),
      'extension' => array(
        'name' => t('Extension'),
      ),
    );
  }
}
